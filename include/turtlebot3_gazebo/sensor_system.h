#ifndef SENSOR_SYSTEM_H_
#define SENSOR_SYSTEM_H_

#include <ros/ros.h>

#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/Odometry.h>

// Define the robot direction of scan
typedef enum DIRECTION {
    FRONT = 0,
    RIGHT,
    LEFT  
} DIRECTION;

//Class for all the sensor system
class SensorSystem
{
public:
    SensorSystem();
    ~SensorSystem();
    //Message Call Back Functions
    void laserScanMsgCallBack(const sensor_msgs::LaserScan::ConstPtr &msg);
    void odomMsgCallBack(const nav_msgs::Odometry::ConstPtr &msg);
    double tb3_pose;
    double prev_tb3_pose;
    double scan[3];

private:
    ros::NodeHandle nh_;
    sensor_msgs::LaserScan laser_msg;
    ros::Subscriber laser_scan_sub_;
    ros::Subscriber odom_sub_;
};

#endif