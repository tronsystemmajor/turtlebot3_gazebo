#include "turtlebot3_gazebo/wall_follower.h"

//Constructor for DriveSystem
DriveSystem::DriveSystem()
{
  ROS_INFO("Drive System Init");
  std::string cmd_vel_topic_name = nh_.param<std::string>("cmd_vel_topic_name", "");
  cmd_vel_pub   = nh_.advertise<geometry_msgs::Twist>(cmd_vel_topic_name, 10);
}

//Destructor for DriveSystem
DriveSystem::~DriveSystem()
{
  updatecommandVelocity(0.0,0.0);
}

//function to set velocities based on the direction
bool DriveSystem:: drive_system(const ROBOT_MOVEMENT move_type)
{
    switch (move_type)
    {
    case STOP:
      ROS_INFO("[ROBOT] STOP! \n");
      updatecommandVelocity(0.0 , 0.0);
      break ;

    case FORWARD:
      ROS_INFO("[ROBOT] FORWARD! \n");
      updatecommandVelocity(0.4 , 0.0);
      break;

    case BACKWARD:
      ROS_INFO("[ROBOT] BACKWARD! \n");
      updatecommandVelocity(-0.75 , 0.0);
      break;

    case TURN_LEFT:
      ROS_INFO("[ROBOT] TURN_LEFT! \n");
      updatecommandVelocity(0.0 , 1.0);
      break;

    case TURN_RIGHT:
        ROS_INFO("[ROBOT] TURN_RIGHT! \n");
        updatecommandVelocity(0.0 , -1.0);  
      break;

    case MOVE_LEFT:
      ROS_INFO("[ROBOT] MOVE_LEFT! \n");
      updatecommandVelocity(0.4 , 0.6);
      break;

    case MOVE_RIGHT:
      ROS_INFO("[ROBOT] MOVE_RIGHT \n");
      updatecommandVelocity(0.3 , -0.3);
      break;
    
    default:
      ROS_INFO("[func drive_system] Invalid move type! \n");
      return false;
      break;
    }
  usleep(10);
  return true;      
}

//function to publish the velocities
void DriveSystem::updatecommandVelocity(double linear, double angular)
{
  geometry_msgs::Twist cmd_vel;

  cmd_vel.linear.x  = linear;
  cmd_vel.angular.z = angular;

  cmd_vel_pub.publish(cmd_vel);
}

