#include "turtlebot3_gazebo/wall_follower.h"

//main function to execute the wall_follower
int main(int argc, char* argv[])
{
  ros::init(argc, argv, "wall_follower");
  WallFollower wall_follower;

  // ros::Rate loop_rate(125);
  ros::Duration time_between_ros_wakeups(0.001);
  while (ros::ok())
  {
    // wall_follower.drive_system(STOP);
    wall_follower.controlLoop();
    ros::spinOnce();
     time_between_ros_wakeups.sleep();
  }

  return 0;
}
