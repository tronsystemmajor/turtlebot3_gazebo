/**
 * WALL FOLLOWER ALGORITHM
 **Team Squirtle
 */

#include "turtlebot3_gazebo/wall_follower.h"

//Control Loop for Wall Following Algorithm
bool WallFollower::controlLoop()
{
double tb3_pose_ = sensor_system_tb3.tb3_pose;
double prev_tb3_pose_ = sensor_system_tb3.prev_tb3_pose;
double front_scan = sensor_system_tb3.scan[FRONT];
double right_scan = sensor_system_tb3.scan[RIGHT];
double left_scan = sensor_system_tb3.scan[LEFT];

//If front is free and closer to wall, go straight
if(front_scan>check_forward_dist && left_scan<=check_side_dist)
{
  ROS_INFO("Following Wall");
  prev_tb3_pose_ = tb3_pose_;
  drive_system_tb3.drive_system(FORWARD);
}

//If front is free and left side is free find a wall
else if(front_scan>check_forward_dist && left_scan>check_side_dist)
{
    ROS_DEBUG("Finding Wall");
    prev_tb3_pose_ = tb3_pose_;
    if (fabs(prev_tb3_pose_ - tb3_pose_) < escape_range){
      drive_system_tb3.drive_system(MOVE_LEFT);
    }
}

//if wall at front 
else if(front_scan<check_forward_dist)
{
  drive_system_tb3.drive_system(STOP);
  //if wall in left then corner detected, turn right
  if(left_scan<=check_side_dist)
  {
    ROS_INFO("LEFT CORNER! \n");
    prev_tb3_pose_ = tb3_pose_;
    if (fabs(prev_tb3_pose_ - tb3_pose_) < escape_range){
      drive_system_tb3.drive_system(TURN_RIGHT);
    }
  }
  //if no wall in left
  else if(left_scan>check_side_dist)
  {
    ROS_INFO("Close to Front wall");
    //if left is free go left 
    if(left_scan>right_scan)
    {
      prev_tb3_pose_ = tb3_pose_;
      if (fabs(prev_tb3_pose_ - tb3_pose_) < escape_range){
        drive_system_tb3.drive_system(TURN_LEFT);
      }
    }
    else if(left_scan==right_scan){
       prev_tb3_pose_ = tb3_pose_;
      if (fabs(prev_tb3_pose_ - tb3_pose_) < escape_range){
        drive_system_tb3.drive_system(TURN_LEFT);
      }
    }
    //if wall in left go right
    else 
    {
      prev_tb3_pose_ = tb3_pose_;
      if (fabs(prev_tb3_pose_ - tb3_pose_) < escape_range)      //adjusting movement based on pose
      {
        drive_system_tb3.drive_system(TURN_RIGHT);
      }
    }
    }
}

//saving the pose information
sensor_system_tb3.tb3_pose = tb3_pose_;
sensor_system_tb3.prev_tb3_pose = prev_tb3_pose_ ;
sensor_system_tb3.scan[FRONT] = front_scan;
sensor_system_tb3.scan[RIGHT] = right_scan;
sensor_system_tb3.scan[LEFT] = left_scan;

return true;

}

